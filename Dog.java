public class Dog
{
	private String name;
	private String breed;
	private int weight;
	
	public  void dogName(String name)
	{
		System.out.println("the dogs' name is "+ name);
	}
	
	public void breedWeight(String name,String breed, int weight)
	{
		System.out.println(name+" is a "+ breed+" breed and weighs "+ weight + "lbs");
	}
	
	public int steak(int meatWeight)
	{
		if (goodSteak(meatWeight))
		{
			this.weight+=meatWeight;
			System.out.println(name +" ate a " + meatWeight +" lbs steak and now weighs " +weight+ "lbs");
		}
		else
		{
			System.out.println("thats way too much meat to be giving your dog");
		}
		return this.weight;
	}
	
	public boolean goodSteak(int meatWeight)
	{
		boolean good=true;
		if(meatWeight<0 || meatWeight>10)
		{
			good=false;
		}
		
		return good;
	}
	//Part 2
	/*public void setName(String name)
	{
		this.name=name;
	}
	*/
	
	public void setBreed(String breed)
	{
		this.breed=breed;
	}
	
	public void setWeight(int weight)
	{
		this .weight=weight;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public String getBreed()
	{
		return this.breed;
	}
	
	public int getWeight()
	{
		return this.weight;
	}
	
	public Dog(String name, String breed, int weight)
	{
		this.name=name;
		this.breed=breed;
		this.weight=weight;
	}
	
	
}